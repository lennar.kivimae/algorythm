document.addEventListener("DOMContentLoaded", function (event) {

    document.querySelectorAll('[data-panel]').forEach((element) => {
        element.addEventListener('click', (event) => {
            event.preventDefault();
            const selectedPanel = event.target.dataset.panel;
            togglePanel(selectedPanel);
        });
    });

    function togglePanel(panel) {
        document.querySelectorAll('[data-section]').forEach((element) => {
            element.classList.remove('is-open');
        });

        document.querySelector(`[data-section="${panel}"]`).classList.add('is-open');
    }

    /* Guessing game */
    document.querySelector('.guessing-game__button').addEventListener('click', (event) => {
        event.preventDefault();
        generateNumbers();
    });

    function generateNumbers() {
        const numbers = [];
        const numberContainer = document.querySelector('.guessing-game__numbers').innerHTML = '';

        for (let index = 1; index < 15; index++) {
            document.querySelector('.guessing-game__numbers').insertAdjacentHTML('beforeend', `<button class="guessing-game__number" data-number="${index}">${index}</button>`);
        }

        document.querySelectorAll('.guessing-game__number').forEach((element) => {
            element.addEventListener('click', (event) => {
                event.preventDefault();
                const selectedNumber = event.target.dataset.number;
                const myNumber = 5;
                event.target.setAttribute('disabled', true);

                if (myNumber > selectedNumber) {
                    alert('My number is larger');

                    return;
                }

                if (myNumber < selectedNumber) {
                    alert('my number is smaller');

                    return;
                }

                alert('w0w, you guessed it');
                document.querySelector('.guessing-game__numbers').innerHTML = '';
            });
        });
    }

    /* Find max number */
    document.querySelector('.find-max__submit').addEventListener('click', (event) => {
        event.preventDefault();
        let numbers = document.querySelector('.find-max input').value;
        numbers = numbers.split(',');
        let largestNumber;

        for (const number of numbers) {
            if (typeof largestNumber === 'undefined' && !isNaN(parseInt(number))) {
                largestNumber = parseInt(number);
            }

            if (parseInt(number) > largestNumber) {
                largestNumber = number;
            }
        }

        if (typeof largestNumber !== 'undefined') {
            alert(`The highest number is: ${largestNumber}`);
            return;
        }

        alert(`You tried to enter a string only didn't you?`);
    });

    /* Is number odd or even */
    document.querySelector('.number-odd__submit').addEventListener('click', (event) => {
        event.preventDefault();
        const number = Number(document.querySelector('.number-odd input').value);
        if (!isNaN(number)) {
            if (number % 2 === 0) {
                alert('Number is even');

                return;
            }

            alert('Number is odd');
            return;
        }

        alert('What you provided is not a number');
    });
});
